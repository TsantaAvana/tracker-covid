import React from 'react';
import PropTypes from 'prop-types';
import IconInstagram from './instagram';
import IconLinkedin from './linkedin';
import IconTwitter from './twitter';
import IconGitHub from './github';
import IconFacebook from './facebook';
import IconSkype from './skype';
import IconTelegram from './telegram';
import IconGmail from "./gmail";
import IconPhon from "./phon";
import IconLocation from "./location";

export const FormattedIcon = ({ name }) => {
  switch (name) {
    case "Instagram":
      return <IconInstagram />;
    case "Linkedin":
      return <IconLinkedin />;
    case "Twitter":
      return <IconTwitter />;
    case "GitHub":
      return <IconGitHub />;
    case "Facebook":
      return <IconFacebook />;
    case "Skype":
      return <IconSkype />;
    case "Telegram":
      return <IconTelegram />;
    case "Phon":
      return <IconPhon />;
    case "Gmail":
      return <IconGmail />;
    case "Location":
      return <IconLocation />;
    default:
      return <IconTwitter />;
  }
};

FormattedIcon.propTypes = {
  name: PropTypes.string.isRequired,
};

export default FormattedIcon;
