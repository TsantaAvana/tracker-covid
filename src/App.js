import React from 'react';
import {HashRouter, Route} from 'react-router-dom';
import Statistique from './components/statistique';
import Acceuil from './components/acceuil';
import Carte from './components/carte';





function App() {
  return (
    <>
    
      <HashRouter>
        <Route exact path="/" component={Acceuil} />
        <Route path="/statistique" component={Statistique} />
        <Route path="/carte" component={Carte} />
      </HashRouter>
  
    
    </>
  );
}

export default App;
