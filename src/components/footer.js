import React from "react";
import styled from "styled-components";
import FormattedIcon from "../icon/formattedIcon";
import { device } from "../style/device";
import { ExternalUrl } from "../utils/url";

const StyledContainer = styled.footer`
  background: #fff;
  width: 100%;
  text-align: left;
`;

const StyledWrapper = styled.div`
  display: flex;
  justify-content: center;
  ${device.bigDesktop`
        display: block;
        padding-left: 2%;
   `};
  ${device.tablet`
        display: block;
        padding-left: 2%;
   `};
  ${device.phone`
        display: block;
        padding-left: 2%;
   `};
  ${device.tiny`
        display: block;
        padding-left: 2%;
   `};
  ${device.phablet`
        display: block;
        padding-left: 2%;
   `};
`;

const StyledLeft = styled.div`
  display: block;
  width: 35%;
  margin-left: 2%;
  ${device.bigDesktop`
        display: block;
        width: 100%;
        margin-left:0;
        margin-bottom: 10px;
        text-align: center;
   `};
  ${device.tablet`
        display: block;
        width: 100%;
        margin-left:0;
        margin-bottom: 10px;
        text-align: center;
   `};
  ${device.phone`
        display: block;
        width: 100%;
        margin-left:0;
        margin-bottom: 10px;
   `};
  ${device.tiny`
        display: block;
        width: 100%;
        margin-left:0;
        margin-bottom: 10px;
   `};
  ${device.phablet`
        display: block;
        width: 100%;
        margin-left:0;
        margin-bottom: 10px;
   `};
`;
const StyledLeftListLink = styled.p`
  display: flex;
  color: #241b57;
  margin: 25px 0;
  font-size: 14px;
  ${device.bigDesktop`
        display: flex;
        justify-content: center;  
        
   `};
  ${device.tablet`
        display: flex;
        justify-content: center;  
        
   `};
  ${device.phone`
        display: flex;
        justify-content: center;  
   `};
  ${device.tiny`
        display: flex;
        justify-content: center;  
   `};
  ${device.phablet`
        display: flex;
        justify-content: center;  
   `};
`;

const StyledLefLink = styled.a`
  text-decoration: none;
  line-height: 1.8;
  color: inherit;
  padding-right: 2em;
  ${device.bigDesktop`
        display: flex;
         
   `};
  ${device.tablet`
        display: flex;
         
   `};
  ${device.phone`
        display: flex;
         
   `};
  ${device.tiny`
        display: flex;
         
   `};
  ${device.phablet`
        display: flex;
         
   `};
`;

const StyledLefCopyright = styled.p`
  color: #ec4357;
  font-size: 14px;
  font-weight: normal;
  margin-top: 30px;
`;

const StyledCenter = styled.div`
  display: block;
  width: 25%;
  margin-left: 2%;
  ${device.bigDesktop`
        width: 100%;
        margin-left:0;
        margin-bottom:10px;
        justify-content: center;
   `};
  ${device.tablet`
        width: 100%;
        margin-left:0;
        margin-bottom:10px;
        justify-content: center;
   `};
  ${device.phone`
        width: 100%;
        margin-left:0;
        margin-bottom:10px;
   `};
  ${device.tiny`
        width: 100%;
        margin-left:0;
        margin-bottom:10px;
   `};
  ${device.phablet`
        width: 100%;
        margin-left:0;
        margin-bottom:10px;
   `};
`;

const StyledCenterWrapper = styled.div`
  display: flex;
  justify-content: left;
  align-items: left;
`;

const StyledCenterText = styled.p`
  vertical-align: middle;
  margin: 0;
`;
const StyledCenterIcon = styled.i`
  vertical-align: middle;
  margin: 10px 15px;
  svg {
    fill: #ec4357;
    width: 20px;
    height: 20px;
  }
`;

const StyledRight = styled.div`
  display: block;
  width: 25%;
  margin-left: 2%;
  ${device.bigDesktop`
        width: 100%;
        margin-left:0;
        
   `};
  ${device.tablet`
        width: 100%;
        margin-left:0;
        
   `};
  ${device.phone`
        width: 100%;
        margin-left:0;
        
   `};
  ${device.tiny`
        width: 100%;
        margin-left:0;
   `};
  ${device.phablet`
        width: 100%;
        margin-left:0;
   `};
`;

const StyledRightConnect = styled.p`
  line-weight: normal;
  color: #241b57;
  font-size: 12px;
  line-height: 20px;
  margin: 0;
  text-align: center;
`;

const StyledRightConnectSpan = styled.span`
  display: block;
  color: #ec4357;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 20px;
`;

const StyledRightIcon = styled.div`
  margin-top: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const StyledLink = styled.a`
  cursor: pointer;
  width: 35px;
  height: 35px;
  font-size: 20px;
  border-radius: 3px;
  text-align: center;
  line-height: 35px;
  svg {
    fill: #ec4357;
    width: 18px;
    height: 18px;
  }
`;

export const Footer = () => {
  return (
    <>
      <StyledContainer>
        <StyledWrapper>
          <StyledLeft>
            <StyledLeftListLink>
              <StyledLefLink
                href={ExternalUrl.about}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="About"
              >
                A propos
              </StyledLefLink>
              <StyledLefLink
                href={ExternalUrl.projet}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="Projets"
              >
                Mes projets
              </StyledLefLink>
              <StyledLefLink
                href={ExternalUrl.competence}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="Competence"
              >
                Mes compétences
              </StyledLefLink>
            </StyledLeftListLink>

            <StyledLefCopyright>
              Covid-Tracker &copy; 2020 - fait par RAKOTONANAHARY Tsanta H'avana
            </StyledLefCopyright>
          </StyledLeft>

          <StyledCenter>
            <StyledCenterWrapper>
              <StyledCenterIcon>
                <FormattedIcon name="Location" />
              </StyledCenterIcon>
              <StyledCenterText>
                <p>
                  <span>Ambohimamory.</span> AB 368III Antanety
                </p>
              </StyledCenterText>
            </StyledCenterWrapper>

            <StyledCenterWrapper>
              <StyledCenterIcon>
                <FormattedIcon name="Phon" />
              </StyledCenterIcon>
              <StyledCenterText>
                <p> +261337492012</p>
              </StyledCenterText>
            </StyledCenterWrapper>

            <StyledCenterWrapper>
              <StyledCenterIcon>
                <FormattedIcon name="Gmail" />
              </StyledCenterIcon>
              <StyledCenterText>
                <p>rtsantah@gmail.com</p>
              </StyledCenterText>
            </StyledCenterWrapper>
          </StyledCenter>

          <StyledRight>
            <StyledRightConnect>
              <StyledRightConnectSpan> Contactez moi </StyledRightConnectSpan>
              Vous pouvez me contacter sur ces differentes réseaux sociaux
            </StyledRightConnect>

            <StyledRightIcon>
              <StyledLink
                href={ExternalUrl.github}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="Github"
              >
                <FormattedIcon name="GitHub" />
              </StyledLink>

              <StyledLink
                href={ExternalUrl.facebook}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="Facebook"
              >
                <FormattedIcon name="Facebook" />
              </StyledLink>

              <StyledLink
                href={ExternalUrl.twitter}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="Twitter"
              >
                <FormattedIcon name="Twitter" />
              </StyledLink>

              <StyledLink
                href={ExternalUrl.instagram}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="Instagram"
              >
                <FormattedIcon name="Instagram" />
              </StyledLink>

              <StyledLink
                href={ExternalUrl.skype}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="Skype"
              >
                <FormattedIcon name="Skype" />
              </StyledLink>

              <StyledLink
                href={ExternalUrl.telegram}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="Telegram"
              >
                <FormattedIcon name="Telegram" />
              </StyledLink>
            </StyledRightIcon>
          </StyledRight>
        </StyledWrapper>
      </StyledContainer>
    </>
  );
};

export default Footer;
