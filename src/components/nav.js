import React from 'react';
import styled from 'styled-components';
import {device} from '../style/device';
import {Link} from 'react-router-dom';



const StyledContainer = styled.header`
  position: relative;
  z-index: 999;
  width: 100%;
  padding: 1.5em 0;
  

`;

const StyledWrapper = styled.div`
  width: 100%;
  max-width: 1100px;
  margin: 0 auto;
  display: flex;
  padding-left: 2%;
  ${device.tablet`
        flex-direction: column;
    `};
  ${device.phone`
        flex-direction: column;
    `};
  ${device.megatiny`
        padding-left:0;
    `};
`;

const StyledName = styled.h2`
    width: 200px;
    padding: 13px 80px 20px 0;
`;


const StyledListLink = styled.ul`
  display: flex;
  margin-right: 5%;
  text-decoration: none;
  list-style-type: none;
  ${device.megatiny`
        margin-right:0;
    `};
`;

const StyledListItem = styled.li`
  padding: 20px;
  text-decoration: none;
  ${device.megatiny`
        padding: 10px;
        padding-top:20px;
    `};
`;

const StyledLink = styled(Link)`
  transition: ease-in 0.2s;
  text-decoration: none;
  color: #606c7c;
  &:hover {
    color: #ec4357;
  }
  
`;

export const Navs = () => {
    return (
      <>
        <StyledContainer>
          <StyledWrapper>
            <StyledName> Uirusu </StyledName>

            <StyledListLink>

              <StyledListItem>
                <StyledLink to="/"> Acceuil </StyledLink>
              </StyledListItem>

              <StyledListItem>
                <StyledLink to="/statistique"> Statistique </StyledLink>
              </StyledListItem>

              <StyledListItem>
                <StyledLink to="/Carte"> Carte </StyledLink>
              </StyledListItem>

            </StyledListLink>
          </StyledWrapper>
        </StyledContainer>
      </>
    );
};

export default Navs;