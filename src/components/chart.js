import React, {useState, useEffect} from "react";
import {fetchDailyData} from '../api/index';
import {Line,Bar} from 'react-chartjs-2';
import styled from 'styled-components';
import { device } from "../style/device";

const StyledContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 70%;
  ${device.tablet`
     width: 100%;
   `};
`;



export const Chart = ({ data: { confirmed,recovered, deaths}, country}) => {
  const [dailyData, setDailyData] = useState([]);

  useEffect(() => {
    const fetchAPI = async () => {
      setDailyData(await fetchDailyData());
    }

    fetchAPI();
  },[]);
  

  const lineChart =
    dailyData.length !== 0 ? (
      <Line
        data={{
          labels: dailyData.map(({ date }) => date),
          datasets: [
            {
              data: dailyData.map(({ confirmed }) => confirmed),
              label: "Infectés",
              borderColor: "#3333ff",
              fill: true
            },
            {
              data: dailyData.map(({ deaths }) => deaths),
              label: "Mort",
              borderColor: "red",
              backgroundColor: "rgba(373,71,92,0.5)",
              fill: true
            }
          ]
        }}
      />
    ) : null;


  const barChart = confirmed ? (
    <Bar
      data={{
        labels: ["Inféctés", "Rétablies", "Morts"],
        datasets: [
          {
            label: "People",
            backgroundColor: [
              "rgba(0,153,255,0.5)",
              "rgba(0,255,0,0.5)",
              "rgba(255,0,0,0.5)"
            ],
            data: [
              confirmed.value,
              recovered.value,
              deaths.value,
            ]
          }
        ]
      }}
      options={{
        legend: { display: false },
        title: { display: true, text: `Situation actuelle à ${country}` }
      }}
    />
  ) : null;

  return (
    <>
      <StyledContainer>
        {country ? barChart : lineChart}
      </StyledContainer>
    </>
  );
};

export default Chart;
