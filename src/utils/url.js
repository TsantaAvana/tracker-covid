export const ExternalUrl = {
         github: "https://github.com/TiavinaRakotobe",
         linkedin:
           "https://www.linkedin.com/in/mamihenintsoa-rakotobe-77a014199/",
         twitter: "https://twitter.com/Tiavina78987904",
         instagram: "https://www.instagram.com/tiavinamamihents/",
         gmail: "mamihenintsoaRakotobe@gmail.com",
         facebook: "https://www.facebook.com/lindow.mind.1",
         skype: "https://join.skype.com/invite/nHze9LISFA9A",
         telegram: "https://t.me/TiavinaRak",
         portfolio: "https://tiavinarakotobe.netlify.app/",
         projet: "https://tiavinarakotobe.netlify.app/#/projects",
         competence: "https://tiavinarakotobe.netlify.app/#/skills",
         about: "https://tiavinarakotobe.netlify.app/#/about",
         articleRester:
           "https://www.futura-sciences.com/sante/actualites/coronavirus-il-important-rester-confine-chez-soi-80071/",
         articleCovid19:
           "https://www.promotionsantevalais.ch/fr/est-ce-coronavirus-1684.html"
       };

export default ExternalUrl;